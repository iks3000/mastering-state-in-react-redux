import GetUsers from './components/GetUsers/GetUsers';
import JoinUs from './components/JoinUs/JoinUs';

function App() {
  return (
    <>
      <GetUsers />
      <JoinUs />
    </>
  );
}

export default App;

import { getUsersStart, getUsersSuccess, getUsersFailure } from './actionTypes';

import axios from 'axios';

export const getUsers = () => {
    return async(dispatch) => {
        try {
            dispatch(getUsersStart());
            const users = await axios.get('/community');
            dispatch(getUsersSuccess(users.data));
        } catch (error) {
            dispatch(getUsersFailure(error));
        }
    };
};
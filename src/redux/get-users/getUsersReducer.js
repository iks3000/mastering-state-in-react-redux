import {
    GET_USERS_START,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE,
} from './actionTypes';

export const initialState = {
    users: [],
    isLoading: false,
    error: '',
};

export const getUsersReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_USERS_START:
            return {
                isLoading: true,
            };
        case GET_USERS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users: payload,
            };
        case GET_USERS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };
        default:
            return state;
    }
};
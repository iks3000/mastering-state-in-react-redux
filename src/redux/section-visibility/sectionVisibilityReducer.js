import { SHOW_SECTION } from './actions';

export const initialState = {
    visible: true,
};
export const sectionVisibilityReducer = (state = initialState, { type }) => {
    switch (type) {
        case SHOW_SECTION:
            return {
                ...state,
                visible: !state.visible,
            };
        default:
            return state;
    }
};
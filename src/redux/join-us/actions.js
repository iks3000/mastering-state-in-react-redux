import {
    subscribeStart,
    subscribeSuccess,
    subscribeFailure,
    unSubscribeStart,
    unSubscribeSuccess,
    unSubscribeFailure,
} from './actionTypes';

import axios from 'axios';

export const subscribe = (email) => {
    return async (dispatch) => {

        try {
            dispatch(subscribeStart());
            const subscribeResponse = await axios.post('/subscribe', email);
            console.log(subscribeResponse)
            if (subscribeResponse.status === 200) {
                dispatch(subscribeSuccess(subscribeResponse.status));
            }
            return subscribeResponse;
        } catch (error) {
            dispatch(subscribeFailure(error.response.data.error));
        }
    };
};

export const unSubscribe = () => {
    return async(dispatch) => {
        try {
            dispatch(unSubscribeStart());
            const unSubscribeResponse = await axios.post('/unsubscribe');
            dispatch(unSubscribeSuccess(unSubscribeResponse.data));
        } catch (error) {
            dispatch(unSubscribeFailure(error));
        }
    };
};
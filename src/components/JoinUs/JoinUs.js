import React, { useEffect, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { subscribe, unSubscribe } from "../../redux/join-us/actions";

import "./JoinUs.scss";

const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

const validate = (email) => {
  const inputEnding = email.substring(email.indexOf("@") + 1);
  return VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
};

const JoinUs = () => {
  const [inputVal, setInputVal] = useState("");
  const [isDisabled, setDisabled] = useState(false);
  const [opacity, setOpacity] = useState(1);
  const [visible, setVisible] = useState(true);

  const dispatch = useDispatch();

  const { error, subscribed } = useSelector(
    ({ success }) => ({
      error: success.error,
      subscribed: success.subscribed,
    }),
    shallowEqual
  );

  let dataEmail = { email: inputVal };

  useEffect(() => {
    const checkSubscribed = () => {
      if (subscribed) {
        setDisabled(false);
        setOpacity(1);
        setInputVal("");
        setVisible(false);
      } else if (error && !subscribed) {
        setDisabled(false);
        setOpacity(1);
        setInputVal("");
        setVisible(false);
        window.alert("Email is already in use");
      }
    };
    const checkUnSubscribed = () => {
      if (!subscribed) {
        setDisabled(false);
        setOpacity(1);
        setVisible(true);
      }
    };
    const waitForResponse = setTimeout(() => {
      checkSubscribed();
      checkUnSubscribed();
    }, 1200);
    return () => clearTimeout(waitForResponse);
  }, [subscribed, error]);

  const clickSubscride = (e) => {
    setDisabled(true);
    setOpacity(0.5);
    e.preventDefault();

    const clickSubscrideChecker = () => {
      if (inputVal === "") {
        window.alert("Type Email address");
        setDisabled(false);
        setOpacity(1);
      } else if (!validate(inputVal)) {
        window.alert("Invalid email, only gmail.com, outlook.com and yandex.ru");
        setDisabled(false);
        setOpacity(1);
        setInputVal("");
      } else {
        setDisabled(true);
        setOpacity(0.5);
        dispatch(subscribe(dataEmail));
      }
    }
    setTimeout(() => { clickSubscrideChecker() }, 100)
  };

  const clickUnsubscribe = () => {
    setDisabled(true);
    setOpacity(0.5);
    dispatch(unSubscribe());
    if (!subscribed) {
      setDisabled(false);
      setOpacity(1);
      setVisible(true);
    }
  };

  return (
    <section className="join-our-program">
      <div className="wrapper">
        <h2 className="join-our-program__title"> Join Our Program </h2>
        <p className="join-our-program__sub-title">
          Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna
          aliqua.
        </p>
        {visible ? (
          <form className="form">
            <input
              value={inputVal}
              onChange={(e) => setInputVal(e.target.value)}
              className="subscribe-input"
              type="email"
              placeholder="Email"
            />
            <button
              className="subscribe-btn"
              type="submit"
              style={{ opacity }}
              onClick={clickSubscride}
              disabled={isDisabled}
            >
              Subscribe
            </button>
          </form>
        ) : (
          <button
            className="subscribe-btn unsubscribe-btn"
              style={{ opacity }}
              disabled={isDisabled}
              onClick={clickUnsubscribe}
          >
            Unsubscribe
          </button>
        )}
      </div>
    </section>
  );
};

export default JoinUs;